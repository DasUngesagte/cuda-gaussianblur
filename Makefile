CC = nvcc

IN =  libppm/libppm.c gaussianblur.cu
OUT = GaussianBlurPPM

all: $(IN)
	$(CC) -Ilibppm/ -o $(OUT) $(IN) -O3 -gencode=arch=compute_75,code=sm_75

debug: $(IN)
	$(CC) -Ilibppm/ -o $(OUT) $(IN) -O3 -gencode=arch=compute_75,code=sm_75 -g

clean:
	rm GaussianBlurPPM

