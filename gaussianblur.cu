#include <cstdint>  // for int-types
#include <cstddef>  // for size_t
#include <cuda.h>
#include <cmath>    // for ceil
#include <iostream> // for cout
#include <string>
#include <vector>
extern "C"{
    #include "libppm.h"
}


#define CHANNELS 3  // RGB

/*********************************************************************************************************************
 *                                               Adjustable                                                          *
 *********************************************************************************************************************/

#define TILES_PER_BLOCK 4

// BLOCK_X * BLOCK_Y should be a multiple of the warp size:
#define BLOCK_X 32
#define BLOCK_Y 32

/*********************************************************************************************************************
 *                                           End of Adjustable                                                       *
 *********************************************************************************************************************/


// Macro for CUDA event time measurements:
#define MEASURE(arg) { cudaEventRecord(stop_##arg);\
    cudaEventSynchronize(stop_##arg);\
    milliseconds_##arg = 0;\
    cudaEventElapsedTime(&milliseconds_##arg, start_##arg, stop_##arg); }\

// Error checking for CUDA:
#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }

inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess) {
      fprintf(stderr,"CUDA Error: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}

/**
 * Creates a 1D gaussian filter.
 * The actual function:
 * https://en.wikipedia.org/wiki/Gaussian_blur#Mathematics
 */
void createGaussian1DFilter(float filter[], float sigma, size_t filter_size) {

    float s = 2.0f * sigma * sigma;
    float factor = 1 / static_cast<float>(sqrt(2 * M_PI * sigma * sigma));
    int64_t mean = filter_size / 2;

    // Generate the filter:
    for (size_t x = 0; x < filter_size; x++) {

        // The middle of the matrix is (0,0), so shift the indices:
        float x1 = static_cast<float>(x) - mean;
        float r = x1 * x1;
        filter[x] = factor * std::exp(-r / s);

    }
}


/*********************************************************************************************************************
 *                                                Kernel                                                             *
 *********************************************************************************************************************/

// Used to handle pixels beyond the edges.
// E.g. if the filter needs value outside the image, return the nearest edge
// index instead.
__device__
int check_edge(int position, int edge) {
    int minimum = position > edge ? edge : position;
    return minimum > 0 ? minimum : 0;
}


/*********************************************************************************************************************
 *                                            Horizontal Pass                                                        *
 *********************************************************************************************************************/

__global__
void convolute_horizontal(const uint8_t *source_channel,
                          uint8_t *target_channel,
                          unsigned image_width,
                          unsigned image_height,
                          const float filter[],
                          int filter_size) {

    // Split the SMEM array into a filter and a tile array:
    extern __shared__ float shared_array[];
    float *filter_smem = shared_array;
    uint8_t *tile_smem = (uint8_t*) &shared_array[filter_size];

    // Give current thread location:
    int local_idx = threadIdx.y * blockDim.x + threadIdx.x;

    // Load filter into SMEM:
    if(local_idx < filter_size){
        filter_smem[local_idx] = filter[local_idx];
    }

    // Constants for the filtering:
    const int radius = filter_size / 2;
    const int tile_width = blockDim.x + 2 * radius;
    const int tile_pos = threadIdx.y * tile_width + threadIdx.x;

    /***************************************** Load the first smem tile **********************************************/

    int global_x = blockIdx.x * blockDim.x * TILES_PER_BLOCK + threadIdx.x;
    const int global_y = blockIdx.y * blockDim.y + threadIdx.y;

    for(int i = 0; i < tile_width; i += blockDim.x){
        const int current_x = check_edge(global_x - radius + i, image_width - 1);  // repeat pixel on the edges
        const int current_pixel_idx = (global_y * image_width + current_x) * CHANNELS;

        // Check whether the window overshoots the later apron:
        if((threadIdx.x + i) < tile_width && global_y < image_height){
            const int smem_idx = (threadIdx.y * tile_width + threadIdx.x + i) * CHANNELS;
            tile_smem[smem_idx + 0] = source_channel[current_pixel_idx + 0];
            tile_smem[smem_idx + 1] = source_channel[current_pixel_idx + 1];
            tile_smem[smem_idx + 2] = source_channel[current_pixel_idx + 2];
        }
    }

    __syncthreads();

    /****************************************** Iterate over the tiles ***********************************************/

    for(int tile = 0; tile < TILES_PER_BLOCK; ++tile){

        global_x = blockIdx.x * blockDim.x * TILES_PER_BLOCK + tile * blockDim.x + threadIdx.x;

        /***************************************** Filter the tile ***********************************************/

        if(global_x >= 0 && global_x < image_width && global_y >= 0 && global_y < image_height){

            // Filter the tile:
            float pixel_r = 0.0f, pixel_g = 0.0f, pixel_b = 0.0f;

            // Actual convolution:
            #pragma unroll 16
            for (int i = 0; i < filter_size; ++i) {
                float filter_val = filter_smem[i];
                pixel_r += tile_smem[(tile_pos + i) * CHANNELS + 0] * filter_val;
                pixel_g += tile_smem[(tile_pos + i) * CHANNELS + 1] * filter_val;
                pixel_b += tile_smem[(tile_pos + i) * CHANNELS + 2] * filter_val;
            }

            // This writes into global memory directly:
            const int global_idx = (global_y * image_width + global_x) * CHANNELS;
            target_channel[global_idx + 0] = (uint8_t) llround(pixel_r);
            target_channel[global_idx + 1] = (uint8_t) llround(pixel_g);
            target_channel[global_idx + 2] = (uint8_t) llround(pixel_b);

        }  // endif

        __syncthreads();  // Sync for memory transfer

        /************************************ Load a tile + apron into SMEM **************************************/

        if(global_x >= 0 && global_x < image_width && global_y >= 0 && global_y < image_height && tile + 1 < TILES_PER_BLOCK){

            // First copy the values already in SMEM, only the last block needs to be loaded from GMEM.
            // We copy the pixel values from the next block into the current.
            // Those are pixels that we can reuse and don't need to load from gmem.
            #pragma unroll
            for(int i = 0; i < tile_width - blockDim.x; i += blockDim.x){

                int smem_idx = threadIdx.y * tile_width + threadIdx.x + i;
                const int smem_idx_next = (smem_idx + blockDim.x) * CHANNELS;
                smem_idx *= CHANNELS;

                if((threadIdx.x + i) < tile_width - blockDim.x){
                    tile_smem[smem_idx + 0] = tile_smem[smem_idx_next + 0];
                    tile_smem[smem_idx + 1] = tile_smem[smem_idx_next + 1];
                    tile_smem[smem_idx + 2] = tile_smem[smem_idx_next + 2];
                }
            }

            // We only need one block of new pixels at the outer edge of the smem tile.
            // So load the *next* block of the image after the current smem tile into the last block
            // of the smem tile:
            const int current_x = check_edge(global_x + radius + blockDim.x, image_width - 1);  // repeat pixel on the edges
            const int current_pixel_idx = (global_y * image_width + current_x) * CHANNELS;
            const int smem_idx = (threadIdx.y * tile_width + threadIdx.x + 2 * radius) * CHANNELS;

            // Load new pixels from gmem:
            tile_smem[smem_idx + 0] = source_channel[current_pixel_idx + 0];
            tile_smem[smem_idx + 1] = source_channel[current_pixel_idx + 1];
            tile_smem[smem_idx + 2] = source_channel[current_pixel_idx + 2];
        }  // endif

        __syncthreads();  // Sync for the next iteration
    }
}


/*********************************************************************************************************************
 *                                             Vertical Pass                                                         *
 *********************************************************************************************************************/

__global__
void convolute_vertical(const uint8_t *source_channel,
                        uint8_t *target_channel,
                        unsigned image_width,
                        unsigned image_height,
                        const float filter[],
                        int filter_size) {

    // Split the SMEM array into a filter and a tile array:
    extern __shared__ float shared_array[];
    float *filter_smem = shared_array;
    uint8_t *tile_smem = (uint8_t*) &shared_array[filter_size];

    // Give current thread location:
    int local_idx = threadIdx.y * blockDim.x + threadIdx.x;

    // Load filter into SMEM:
    if(local_idx < filter_size){
        filter_smem[local_idx] = filter[local_idx];
    }

    // Constants for the filtering:
    const int radius = filter_size / 2;
    const int tile_height = blockDim.y + 2 * radius;

    /***************************************** Load the first smem tile **********************************************/

    const int global_x = blockIdx.x * blockDim.x + threadIdx.x;
    int global_y = blockIdx.y * blockDim.y * TILES_PER_BLOCK + threadIdx.y;


    for(int i = 0; i < tile_height; i += blockDim.y){
        const int current_y = check_edge(global_y - radius + i, image_height - 1);  // repeat pixel on the edges
        const int current_pixel_idx = (current_y * image_width + global_x) * CHANNELS;

        // Check whether the window overshoots the later apron:
        if((threadIdx.y + i) < tile_height && global_x < image_width){
            const int smem_idx = ((threadIdx.y + i) * blockDim.x + threadIdx.x) * CHANNELS;
            tile_smem[smem_idx + 0] = source_channel[current_pixel_idx + 0];
            tile_smem[smem_idx + 1] = source_channel[current_pixel_idx + 1];
            tile_smem[smem_idx + 2] = source_channel[current_pixel_idx + 2];
        }
    }

    __syncthreads();

    /****************************************** Iterate over the tiles ***********************************************/

    for(int tile = 0; tile < TILES_PER_BLOCK; ++tile){

        global_y = blockIdx.y * blockDim.y * TILES_PER_BLOCK + tile * blockDim.y + threadIdx.y;

        /***************************************** Filter the tile ***********************************************/

        if(global_x >= 0 && global_x < image_width && global_y >= 0 && global_y < image_height){

            // Filter the tile:
            float pixel_r = 0.0f, pixel_g = 0.0f, pixel_b = 0.0f;

            // Actual convolution:
            #pragma unroll 16
            for (int i = 0; i < filter_size; ++i) {
                const int tile_pos = ((threadIdx.y + i) * blockDim.x + threadIdx.x) * CHANNELS;
                float filter_val = filter_smem[i];
                pixel_r += tile_smem[tile_pos + 0] * filter_val;
                pixel_g += tile_smem[tile_pos + 1] * filter_val;
                pixel_b += tile_smem[tile_pos + 2] * filter_val;

            }

            // This writes into global memory directly:
            const int global_idx = (global_y * image_width + global_x) * CHANNELS;
            target_channel[global_idx + 0] = (uint8_t) llround(pixel_r);
            target_channel[global_idx + 1] = (uint8_t) llround(pixel_g);
            target_channel[global_idx + 2] = (uint8_t) llround(pixel_b);

        }  // endif

        __syncthreads();  // Sync for memory transfer

        /************************************ Load a tile + apron into SMEM **************************************/

        if(global_x >= 0 && global_x < image_width && global_y >= 0 && global_y < image_height && tile + 1 < TILES_PER_BLOCK){

            // First copy the values already in SMEM, only the last block needs to be loaded from GMEM.
            // We copy the pixel values from the next block into the current.
            // Those are pixels that we can reuse and don't need to load from gmem.
            #pragma unroll
            for(int i = 0; i < tile_height - blockDim.y; i += blockDim.y){

                const int smem_idx = ((threadIdx.y + i) * blockDim.x + threadIdx.x) * CHANNELS;
                const int smem_idx_next = ((threadIdx.y + i + blockDim.y) * blockDim.x + threadIdx.x) * CHANNELS;

                if((threadIdx.y + i) < tile_height - blockDim.y){
                        tile_smem[smem_idx + 0] = tile_smem[smem_idx_next + 0];
                        tile_smem[smem_idx + 1] = tile_smem[smem_idx_next + 1];
                        tile_smem[smem_idx + 2] = tile_smem[smem_idx_next + 2];
                }
            }

            // We only need one block of new pixels at the outer edge of the smem tile.
            // So load the *next* block of the image after the current smem tile into the last block
            // of the smem tile:
            const int current_y = check_edge(global_y + radius + blockDim.y, image_height - 1);  // repeat pixel on the edges
            const int current_pixel_idx = (current_y * image_width + global_x) * CHANNELS;
            const int smem_idx = ((threadIdx.y + 2 * radius) * blockDim.x + threadIdx.x) * CHANNELS;

            // Load new pixels from gmem:
            tile_smem[smem_idx + 0] = source_channel[current_pixel_idx + 0];
            tile_smem[smem_idx + 1] = source_channel[current_pixel_idx + 1];
            tile_smem[smem_idx + 2] = source_channel[current_pixel_idx + 2];
        }  // endif

        __syncthreads();  // Sync for the next iteration
    }
}


/*********************************************************************************************************************
 *                                                    Rest                                                           *
 *********************************************************************************************************************/

/**
 * Loads the image at the given path into the vector in a RGB-pattern
 */
void loadImage(std::vector<uint8_t> &image_source,
               unsigned &image_width,
               unsigned &image_height,
               const std::string &input_path) {

    PPM_Image *image = loadPPM(input_path.c_str());
    image_width = image->image_width;
    image_height = image->image_height;
    image_source = std::vector<uint8_t>(image->data, image->data + image_width * image_height * CHANNELS);
    deletePPM(image);
}


/**
 * Saves the image at the given path into the vector in a RGB-pattern
 */
void saveImage(const uint8_t *image_source,
               const unsigned image_width,
               const unsigned image_height,
               const std::string &output_path) {

    PPM_Image *image = createPPM(P6, image_width, image_height, 255, (unsigned char*) image_source);
    savePPM(output_path.c_str(), image);
    deletePPM(image);

}


/**
 * Will init everything and start the convolution
 */
void setupImageFiltering(std::string input_path, std::string output_path, double sigma) {

    // Load the image:
    std::vector<uint8_t> image_source;
    unsigned image_width;
    unsigned image_height;
    loadImage(image_source, image_width, image_height, input_path);
    unsigned complete_image_size = CHANNELS * image_width * image_height;

    // Calculate the gaussian filter matrix:
    size_t filter_size = static_cast<size_t>(ceil(6.0 * sigma)) | 1;
    auto *filter = new float[filter_size];
    createGaussian1DFilter(filter, sigma, filter_size);

    /***************************************************************************
     *      Reserve memory at host for the source image to read from and
     *      the target image for the device to write its results into
     **************************************************************************/

    auto target_image = new uint8_t[complete_image_size];

    // Timekeeping events:
    cudaEvent_t start_upload, stop_upload, start_download, stop_download;
    cudaEvent_t start_vert, stop_vert, start_hori, stop_hori;
    cudaEventCreate(&start_upload); cudaEventCreate(&stop_upload);
    cudaEventCreate(&start_download); cudaEventCreate(&stop_download);
    cudaEventCreate(&start_vert); cudaEventCreate(&stop_vert);
    cudaEventCreate(&start_hori); cudaEventCreate(&stop_hori);
    float milliseconds_upload, milliseconds_download, milliseconds_vert, milliseconds_hori;

    std::cout << "Starting filtering...\n" << std::endl;

    float* filter_gpu;          gpuErrchk(cudaMalloc(&filter_gpu, filter_size * sizeof(float)));
    uint8_t* source_image_gpu;  gpuErrchk(cudaMalloc(&source_image_gpu, complete_image_size * sizeof(uint8_t)));
    uint8_t* target_image_gpu;  gpuErrchk(cudaMalloc(&target_image_gpu, complete_image_size * sizeof(uint8_t)));

    /***************************************************************************
     *      Upload data:
     **************************************************************************/

    // Start the timer (we also time memory transfer):
    cudaEventRecord(start_upload);

    gpuErrchk(cudaMemcpy(filter_gpu, filter, filter_size * sizeof(float), cudaMemcpyHostToDevice));
    gpuErrchk(cudaMemcpy(source_image_gpu, &image_source[0], complete_image_size * sizeof(uint8_t), cudaMemcpyHostToDevice));

    MEASURE(upload);

    /***************************************************************************
     *      Start horizontal convolution:
     **************************************************************************/

    cudaEventRecord(start_hori);

    // Warp-size many threads per block:
    dim3 threadsPerBlock(BLOCK_X, BLOCK_Y);

    // Calculate # of threads needed on each axis:
    unsigned tiles_needed_x = ceil(static_cast<double>(image_width)  / BLOCK_X);
    unsigned tiles_needed_y = ceil(static_cast<double>(image_height) / BLOCK_Y);
    dim3 numBlocks(ceil(static_cast<double>(tiles_needed_x) / TILES_PER_BLOCK),
                   ceil(tiles_needed_y));

    // Do the actual convolution:
    convolute_horizontal<<<numBlocks,
                           threadsPerBlock,
                           filter_size * sizeof(float) + BLOCK_Y * (BLOCK_X + 2 * (filter_size/2)) * CHANNELS * sizeof(uint8_t)
                        >>>(source_image_gpu, target_image_gpu,image_width, image_height, filter_gpu, filter_size);

    gpuErrchk(cudaGetLastError());
    MEASURE(hori);

    /***************************************************************************
     *      Start vertical convolution:
     **************************************************************************/

    cudaEventRecord(start_vert);

    // Change data around, the partial convoluted image is now in target_image_gpu_*:
    numBlocks = dim3(ceil(tiles_needed_x),
                     ceil(static_cast<double>(tiles_needed_y) / TILES_PER_BLOCK));
    convolute_vertical<<<numBlocks,
                           threadsPerBlock,
                           filter_size * sizeof(float) + BLOCK_Y * (BLOCK_X + 2 * (filter_size/2)) * CHANNELS * sizeof(uint8_t)
                        >>>(target_image_gpu, source_image_gpu, image_width, image_height, filter_gpu, filter_size);
    gpuErrchk(cudaGetLastError());
    MEASURE(vert);

    /***************************************************************************
     *      Download data:
     **************************************************************************/

    cudaEventRecord(start_download);

    gpuErrchk(cudaMemcpy(target_image, source_image_gpu, complete_image_size * sizeof(uint8_t), cudaMemcpyDeviceToHost));

    MEASURE(download);

    /***************************************************************************
     *      Perform the last steps and save the image:
     **************************************************************************/

    std::cout << "Elapsed GPU time:" << std::endl;
    std::cout << "\tUpload:\t\t\t" << milliseconds_upload << " ms" << std::endl;
    std::cout << "\tHorizontal kernel:\t" << milliseconds_hori << " ms"  << std::endl;
    std::cout << "\tVertical kernel:\t" << milliseconds_vert << " ms"  << std::endl;
    std::cout << "\tDownload:\t\t" << milliseconds_download << " ms" << std::endl;

    std::cout << "\nWriting image...\n" << std::endl;

    // Save the image:
    std::cout << "Saving image...\n" << std::endl;
    saveImage(target_image, image_width, image_height, output_path);

    // Free allocated ressources:
    cudaFree(filter_gpu);
    cudaFree(source_image_gpu);
    cudaFree(target_image_gpu);
    delete[] filter;
    delete[] target_image;
}


/*********************************************************************************************************************
 *                                                     Main                                                          *
 *********************************************************************************************************************/

int main(int argc, char *argv[]) {

    if (argc < 4) {
        std::cout << "Usage: ./GaussianBlurPPM input.png output.png sigma"
                  << std::endl;
        exit(EXIT_FAILURE);
    }

    std::string input_path(argv[argc - 3]);
    std::string output_path(argv[argc - 2]);
    double sigma = atof(argv[argc - 1]);

    setupImageFiltering(input_path, output_path, sigma);
}
